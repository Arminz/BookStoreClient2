package com.example.armin.storebookback4.RetrofitHandlers.Callers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.armin.storebookback4.Interfaces.LogoutEvents;
import com.example.armin.storebookback4.Interfaces.SignUpEvents;
import com.example.armin.storebookback4.KEYS;
import com.example.armin.storebookback4.Interfaces.LoginEvents;
import com.example.armin.storebookback4.Models.LoginResponse;
import com.example.armin.storebookback4.RetrofitHandlers.ErrorHandlers.APIError;
import com.example.armin.storebookback4.RetrofitHandlers.ErrorHandlers.ErrorUtils;
import com.example.armin.storebookback4.RetrofitHandlers.ServiceGenerator;
import com.example.armin.storebookback4.User;
import com.example.armin.storebookback4.APIs.UserApi;
import com.example.armin.storebookback4.UserSharedPreferencesHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by armin on 2/6/17.
 */
public class UserStateHandler {
    private Context context;
    private UserSharedPreferencesHandler sharedPreferencesHandler;
    public UserStateHandler(Context context){
        this.context = context;
        this.sharedPreferencesHandler = new UserSharedPreferencesHandler(context);

    }

    public void loginUser(String username, String password)
    {
        loginUser(username,password,null);
    }

    public void loginUser(final String username, final String password, @Nullable  final LoginEvents le)
    {
        final UserApi service = ServiceGenerator.createService(UserApi.class,username,password);

//        User user = new User(username,password);
        Log.d(KEYS.LOG.RETROFIT, "going to login " + username);
        Call<LoginResponse> call = service.loginUser();

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    Log.d(KEYS.LOG.RETROFIT, "login shod : " + response.message());
                    if (le != null)
                        le.onLoginCompleted(response.raw().toString());
                    UserSharedPreferencesHandler usp = new UserSharedPreferencesHandler(context);
                    usp.commitUser(username,password);

                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    if (le != null)
                        le.onLoginFailed(error.message());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d(KEYS.LOG.RETROFIT,"fail : " + t.getMessage());
                if (le != null)
                    le.onLoginFailed(t.toString());

            }
        });
    }

    public void logoutUser(){
        logoutUser(null);
    }

    public void logoutUser(@Nullable final LogoutEvents le)
    {
        UserApi server = ServiceGenerator.createService(UserApi.class);
        Call<Boolean> call = server.logoutUser();

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()){
                    Log.d(KEYS.LOG.RETROFIT,"successful : " + response.body());
                    if (le != null)
                        le.onLogoutSuccessful();

                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    if (le != null)
                        le.onLogoutFailed(error.message());
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.d(KEYS.LOG.RETROFIT,"fail : " + t.toString());
            }
        });
        sharedPreferencesHandler.removeUser();


    }
    public void registerUser(String username, String password)
    {
        registerUser(username,password,null);
    }
    public void registerUser(String username, String password,@Nullable final SignUpEvents se)
    {

        UserApi service = ServiceGenerator.createService(UserApi.class);

        User user = new User(username,password);
        Log.d(KEYS.LOG.RETROFIT, "going to register " + username);
        Call<User> call = service.registerUser(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    Log.d("tag", user.getUsername() + "is registered.");
                    if (se != null)
                        se.onSignUpCompleted();

                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    if (se != null)
                        se.onSignUpFailed(error.message());

                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(KEYS.LOG.RETROFIT, "error : " + t.getMessage());
                if (se != null)
                    se.onSignUpFailed(t.getMessage());


            }
        });

//        sharedPreferencesHandler.commitUser(username,password);

    }
}
