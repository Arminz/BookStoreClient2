package com.example.armin.storebookback4.RetrofitHandlers.Callers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.armin.storebookback4.APIs.ApiInterface;
import com.example.armin.storebookback4.Interfaces.QueryBook;
import com.example.armin.storebookback4.Interfaces.callBooksEvents;
import com.example.armin.storebookback4.Models.Book;
import com.example.armin.storebookback4.RetrofitHandlers.ErrorHandlers.APIError;
import com.example.armin.storebookback4.RetrofitHandlers.ErrorHandlers.ErrorUtils;
import com.example.armin.storebookback4.RetrofitHandlers.ServiceGenerator;
import com.example.armin.storebookback4.UserSharedPreferencesHandler;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by armin on 2/7/17.
 */
public class BooksCaller {
    Context context;
    String username,password;
    ApiInterface service;
    public BooksCaller(Context context)
    {
        this.context = context;
        UserSharedPreferencesHandler usp = new UserSharedPreferencesHandler(context);
        this.username = usp.getUsername();
        this.password = usp.getPassword();
        this.service = ServiceGenerator.createService(ApiInterface.class,username,password);

    }


    public void callBooks(@NonNull final callBooksEvents re){

        Log.d("tag" , username + " : " + password);

        Call<List<Book>> books = service.getBooks();
        books.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                if (response.isSuccessful()){
                    re.onResponseSuccessful(response.body());
                    
                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    re.onResponseFailed(error.message());
                }

            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                re.onResponseFailed(t.getMessage());
            }
        });

    }
    public void reserveBook(Long bookId , @Nullable final QueryBook qb)
    {
        Log.d("tag" , username + " : " + password);

        Call<Void> call = service.reserveBook(bookId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (qb != null) {
                    if (response.isSuccessful())
                        qb.onSuccess();
                    else {
                        APIError error = ErrorUtils.parseError(response);
                        qb.onFailure(error.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (qb != null)
                    qb.onFailure(t.getMessage());

            }
        });
    }
    public void retainBook(Long bookId, @Nullable final QueryBook qb)
    {
        Log.d("tag" , username + " : " + password);
        Call<Void> call = service.retainBook(bookId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (qb != null) {
                    if (response.isSuccessful())
                        qb.onSuccess();
                    else {
                        APIError error = ErrorUtils.parseError(response);
                        qb.onFailure(error.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (qb != null)
                    qb.onFailure(t.getMessage());

            }
        });

    }
}
