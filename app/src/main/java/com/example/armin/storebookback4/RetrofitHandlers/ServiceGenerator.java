package com.example.armin.storebookback4.RetrofitHandlers;

import android.text.TextUtils;
import android.util.Log;

import com.example.armin.storebookback4.AuthenticationInterceptor;
import com.example.armin.storebookback4.KEYS;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by armin on 2/4/17.
 */
public class ServiceGenerator {
    public static final String API_BASE_URL = KEYS.URL.BASE;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit retrofit;

    public static Retrofit retrofit() {
        OkHttpClient client = httpClient.build();
        return builder.client(client).build();
    }


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        Log.d(KEYS.LOG.RETROFIT,"creates service null null");
        return createService(serviceClass, null, null);
    }

    public static <S> S createService(
            Class<S> serviceClass, String username, String password) {
        Log.d(KEYS.LOG.RETROFIT,"create service user,pass: " + username + ":" + password);
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(serviceClass, authToken);
        }

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);



        return builder.client(httpClient.build()).build().create(serviceClass);
    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {

        Log.d(KEYS.LOG.RETROFIT,"create service auth : " + authToken);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
                return builder.build().create(serviceClass);
            }
        }

        Log.d(KEYS.LOG.RETROFIT,"holly shit");
        return builder.build().create(serviceClass);
        //https://futurestud.io/tutorials/android-basic-authentication-with-retrofit//

    }
}
