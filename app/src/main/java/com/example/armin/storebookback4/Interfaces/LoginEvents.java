package com.example.armin.storebookback4.Interfaces;

/**
 * Created by armin on 2/6/17.
 */
public interface LoginEvents {

    public void onLoginCompleted(String response);
    public void onLoginFailed(String error);

}
