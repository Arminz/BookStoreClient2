package com.example.armin.storebookback4.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.armin.storebookback4.Interfaces.LoginEvents;
import com.example.armin.storebookback4.Interfaces.SignUpEvents;
import com.example.armin.storebookback4.R;
import com.example.armin.storebookback4.RetrofitHandlers.Callers.UserStateHandler;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {


    @Bind(R.id.username_editText)EditText usernameEditText;
    @Bind(R.id.password_editText)EditText passwordEditText;
    @Bind(R.id.login_button) Button loginButton;
    @Bind(R.id.register_button) Button registerButton;

    UserStateHandler userStateHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        usernameEditText.setText("user");
        passwordEditText.setText("password");
        userStateHandler = new UserStateHandler(getApplicationContext());


    }
    @OnClick(R.id.register_button)
    public void registerUser()
    {
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        userStateHandler.registerUser(username, password,new SignUpEvents() {
            @Override
            public void onSignUpCompleted() {
                Toast.makeText(getApplicationContext(),"user is registered now. " , Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSignUpFailed(String error) {
                Toast.makeText(LoginActivity.this, "failed :  " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    @OnClick(R.id.login_button)
    public void loginUser()
    {
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        userStateHandler.loginUser(username, password,new LoginEvents() {
            @Override
            public void onLoginCompleted(String response) {
                startActivity(new Intent(LoginActivity.this,BooksActivity.class));
                finish();
            }

            @Override
            public void onLoginFailed(String error) {
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
            }
        });

    }



}

