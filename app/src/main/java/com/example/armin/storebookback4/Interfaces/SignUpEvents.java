package com.example.armin.storebookback4.Interfaces;

/**
 * Created by armin on 2/6/17.
 */
public interface SignUpEvents {
    public void onSignUpCompleted();
    public void onSignUpFailed(String error);
}
