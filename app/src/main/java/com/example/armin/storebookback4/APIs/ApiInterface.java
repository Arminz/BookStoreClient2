package com.example.armin.storebookback4.APIs;

import com.example.armin.storebookback4.Models.Book;
import com.example.armin.storebookback4.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by armin on 2/1/17.
 */

public interface ApiInterface {
    @POST("book/")
    Call<List<Book>> getBooks();
    @POST("book/reserve")
    Call<Void> reserveBook(@Body Long bookId);

    @POST("book/retain")
    Call<Void> retainBook(@Body Long bookId);

    @GET("person")
    Call<User> createPerson(@Query("username") String username);

}
