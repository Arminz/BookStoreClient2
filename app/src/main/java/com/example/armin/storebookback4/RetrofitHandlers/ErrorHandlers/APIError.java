package com.example.armin.storebookback4.RetrofitHandlers.ErrorHandlers;

/**
 * Created by armin on 2/6/17.
 */
public class APIError {
    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

}
