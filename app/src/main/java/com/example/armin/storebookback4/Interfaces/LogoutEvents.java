package com.example.armin.storebookback4.Interfaces;

/**
 * Created by armin on 2/7/17.
 */
public interface LogoutEvents {
    public void onLogoutSuccessful();
    public void onLogoutFailed(String error);
}
