package com.example.armin.storebookback4.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Toast;

import com.example.armin.storebookback4.Adapters.BooksAdapter;
import com.example.armin.storebookback4.Interfaces.LogoutEvents;
import com.example.armin.storebookback4.Interfaces.callBooksEvents;
import com.example.armin.storebookback4.Models.Book;
import com.example.armin.storebookback4.R;
import com.example.armin.storebookback4.RetrofitHandlers.Callers.BooksCaller;
import com.example.armin.storebookback4.RetrofitHandlers.Callers.UserStateHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BooksActivity extends AppCompatActivity {

//    @Bind(R.id.swipeRefresh)
//    SwipeRefreshLayout swl;

    @Bind(R.id.get_books_button)
    Button getBooksButton;

    @Bind(R.id.books_recyclerView)
    RecyclerView recyclerView;

    private BooksAdapter booksAdapter;

    public List<Book> bookList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        ButterKnife.bind(this);
        recyclerView.setHasFixedSize(true);
        bookList = new ArrayList<>();
//        booksAdapter = new BooksAdapter(bookList,R.layout.book_item);
        booksAdapter = new BooksAdapter(bookList,R.layout.book_card);
        recyclerView.setAdapter(booksAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());



//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


//        swl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                getBooks();
//            }
//        });



    }

//
    @OnClick(R.id.get_books_button)
    public void getBooks()
    {
        BooksCaller booksCaller = new BooksCaller(getApplicationContext());
        booksCaller.callBooks(new callBooksEvents() {
            @Override
            public void onResponseSuccessful(List<Book> gottenBookList) {
                bookList = gottenBookList;
                booksAdapter.reloadData(gottenBookList);
            }

            @Override
            public void onResponseFailed(String error) {
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
//                Snackbar snackbar = Snackbar.make(null,error,Snackbar.LENGTH_SHORT);
//                snackbar.show();
            }
        });
    }
    @OnClick(R.id.logout_button)
    public void logout()
    {
        UserStateHandler ush = new UserStateHandler(getApplicationContext());
        ush.logoutUser(new LogoutEvents() {
            @Override
            public void onLogoutSuccessful() {
                finish();
                startActivity(new Intent(BooksActivity.this,LoginActivity.class));
            }

            @Override
            public void onLogoutFailed(String error) {
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
            }
        });
        }

}
