package com.example.armin.storebookback4;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by armin on 2/5/17.
 */
public class UserSharedPreferencesHandler {
    SharedPreferences sharedPreferences;
    public UserSharedPreferencesHandler(Context context)
    {
        this.sharedPreferences = context.getSharedPreferences(KEYS.SHARED_PREFERENCES.USER_SEC,Context.MODE_PRIVATE);
    }
    public void commitUser(String username,String password){
        Log.d(KEYS.LOG.SHARED_PREFERENCES, String.format("going to store %s : %s" ,username,password));
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEYS.USER.USERNAME,username);
        editor.putString(KEYS.USER.PASSWORD,password);
        editor.apply();
        Log.d(KEYS.LOG.SHARED_PREFERENCES, "applied.");
    }
    public String getUsername()
    {
        return sharedPreferences.getString(KEYS.USER.USERNAME,"NO USER");
    }
    public String getPassword()
    {
        return sharedPreferences.getString(KEYS.USER.PASSWORD,"NO PASSWORD");
    }
    public void removeUser()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(KEYS.USER.USERNAME);
        editor.remove(KEYS.USER.PASSWORD);
        editor.apply();
    }


}
