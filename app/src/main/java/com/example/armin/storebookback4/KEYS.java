package com.example.armin.storebookback4;

/**
 * Created by armin on 2/1/17.
 */
public interface KEYS {


    public interface URL
    {
//        public String BASE= "http://192.168.43.46:8080/";
        public String BASE= "http://192.168.10.60:8080/";

    }
    public interface LOG
    {

        public String RETROFIT = "Retrofit";
        public String SHARED_PREFERENCES = "SharedPreferences";
        public String BOOKS_ADAPTER = "BooksAdapter";

    }
    public interface SHARED_PREFERENCES{
        public String USER_SEC = "user_sec";
    }

    public interface USER{
        public String USERNAME = "USERNAME";
        public String PASSWORD = "PASSWORD";


    }
}
