package com.example.armin.storebookback4.APIs;

import com.example.armin.storebookback4.Models.LoginResponse;
import com.example.armin.storebookback4.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by armin on 2/4/17.
 */
public interface UserApi {
    @POST("user/add")
    Call<User> registerUser(@Body User user);

    @GET("user/exists/{username}")
    Call<Boolean> isUserExists(@Path("username") String username);

    @POST("/user/login")
    Call<LoginResponse> loginUser();

    @GET("/user/logout")
    Call<Boolean> logoutUser();

}
