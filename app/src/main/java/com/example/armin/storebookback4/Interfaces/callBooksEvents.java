package com.example.armin.storebookback4.Interfaces;

import com.example.armin.storebookback4.Models.Book;

import java.util.List;

/**
 * Created by armin on 2/7/17.
 */
public interface callBooksEvents {
    public void onResponseSuccessful(List<Book> books);
    public void onResponseFailed(String error);
}
